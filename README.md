## Сборка

На машине должна быть предуставноленна java17
* Сборка запускается командой `mvnw clean package`

## Запуск

* Запуск производится командой `java -jar ./target/demo-spasibo-0.0.1-SNAPSHOT.jar`, по умолчанию запускается на порту `8080`.

## Примечания

* При запуске создается 6 пользователей из них:
  * 3 пользователя с правами `ROLE_CUSTOMER` - `custmoer1`, `customer2`, `customer3`
  * 2 пользова с правами `ROLE_RECEIVER` - `shop`, `cinema`
  * 1 пользователь с правами `ROLE_BANK` - `sber`
  * Все пользователи имеют один пароль `password`
* Роли имеют сдледующие доступы
  *  `ROLE_CUSTOMER` - возможность иметь `Spasibo`, а так же просматривать свои баллы.
  *  `ROLE_RECEIVER` - возможность просамтривать баллы `Spasibo` других пользователей, а так же списывать их.
  *  `ROLE_BANK` - возможность просамтривать баллы `Spasibo` других пользователей, а так же увеличивать их.
* Эндпойнты
  * `for_me` - GET-запрос для пользователей с правами `ROLE_CUSTOMER`. Позволяет проверить свои баллы `Spasibo`.
    * Пример запроса: `curl --location 'http://localhost:8080/for_me' --header 'Authorization: Basic Y3VzdG9tZXIxOnBhc3N3b3Jk'`
  * `by_user/<user-id>` -  GET-запрос для пользователей с правами `ROLE_RECEIVER` и `ROLE_BANK`. Позволяет проверить баллы `Spasibo` других пользователей.
    * Пример запроса: `curl --location 'http://localhost:8080/by_user/1' --header 'Authorization: Basic c2hvcDpwYXNzd29yZA=='`
  * `add` - POST-запрос для пользователей с правами `ROLE_BANK`. Позволяет добавлять баллы `Spasibo` пользователям.
    * Пример запроса: `curl --location 'http://localhost:8080/add' --header 'Content-Type: application/json' --header 'Authorization: Basic c2JlcjpwYXNzd29yZA==' --data '{ "userId": 1, "changed": 100 }'`
  * `reduce` - POST-запрос для пользователей с правами `ROLE_RECEIVER`. Позволяет вычитать баллы `Spasibo` пользователей.
    * Пример запроса: `curl --location 'http://localhost:8080/reduce' --header 'Content-Type: application/json' --header 'Authorization: Basic c2hvcDpwYXNzd29yZA==' --data '{ "userId": 1, "changed": 34 }'`