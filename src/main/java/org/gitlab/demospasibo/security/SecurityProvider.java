package org.gitlab.demospasibo.security;

import lombok.AllArgsConstructor;
import org.gitlab.demospasibo.models.Role;
import org.gitlab.demospasibo.models.User;
import org.gitlab.demospasibo.ropsitories.UserRepo;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public class SecurityProvider implements AuthenticationProvider {

    private final UserRepo userRepo;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String login = (String) authentication.getPrincipal();
        Optional<User> optUser = userRepo.findByName(login);
        User user = optUser.orElseThrow(
            () -> new BadCredentialsException("User with login " + login + " not found")
        );
        if (!BCrypt.checkpw(
                (String) authentication.getCredentials(),
                user.getPassword())
        ) {
            throw new BadCredentialsException("Bad password for user " + login);
        }
        BasicAuth basicAuth = new BasicAuth(
            user,
            Stream.of(user.getRole())
                .map(Role::getRoleName)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList())
        );
        basicAuth.setAuthenticated(true);
        return basicAuth;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(clazz);
    }
}
