package org.gitlab.demospasibo.constants;

public class Roles {
    public static final String CUSTOMER = "ROLE_CUSTOMER";
    public static final String RECEIVER = "ROLE_RECEIVER";
    public static final String BANK = "ROLE_BANK";
}
