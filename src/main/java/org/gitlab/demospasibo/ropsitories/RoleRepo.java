package org.gitlab.demospasibo.ropsitories;

import org.gitlab.demospasibo.models.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends CrudRepository<Role, Long> {
}
