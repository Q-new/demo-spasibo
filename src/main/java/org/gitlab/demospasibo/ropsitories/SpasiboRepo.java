package org.gitlab.demospasibo.ropsitories;

import org.gitlab.demospasibo.models.Spasibo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpasiboRepo extends CrudRepository<Spasibo, Long> {
    Optional<Spasibo> findByUserId(Long id);
}
