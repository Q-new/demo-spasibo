package org.gitlab.demospasibo.exceptions;

public class BadApiException extends RuntimeException{
    public BadApiException(String message) {
        super(message);
    }
}
