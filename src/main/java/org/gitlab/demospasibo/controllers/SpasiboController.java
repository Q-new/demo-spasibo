package org.gitlab.demospasibo.controllers;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.gitlab.demospasibo.dto.SpasiboDto;
import org.gitlab.demospasibo.models.User;
import org.gitlab.demospasibo.services.SpasiboService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

import static org.gitlab.demospasibo.constants.Roles.*;

@RestController("spasibo")
@AllArgsConstructor
@Slf4j
public class SpasiboController {

    private final SpasiboService spasiboService;

    @PostMapping(
        path = "add",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured(BANK)
    public CompletableFuture<ResponseEntity<Double>> accumulateSpasiboByUser(
        @Valid
        @RequestBody
        SpasiboDto dto
    ) {
        return CompletableFuture.supplyAsync(() -> spasiboService.addSpasiboPointsByUser(dto))
            .thenApply(ResponseEntity::ok);
    }

    @GetMapping(path = "by_user/{id}")
    @Secured({RECEIVER, BANK})
    public CompletableFuture<ResponseEntity<Double>> getSpasiboByUser(
        @PathVariable Long id
    ) {
        return CompletableFuture.supplyAsync(() -> spasiboService.getSpasiboPointsByUser(id))
            .thenApply(ResponseEntity::ok);
    }

    @GetMapping(
        path = "for_me",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured(CUSTOMER)
    public CompletableFuture<ResponseEntity<Double>> getSpasiboForMe(Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        return getSpasiboByUser(user.getId());
    }

    @PostMapping(
        path = "reduce",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured(RECEIVER)
    public CompletableFuture<ResponseEntity<Double>> reduceSpasiboByUser(
        @Valid
        @RequestBody
        SpasiboDto dto
    ) {
        return CompletableFuture.supplyAsync(() -> spasiboService.reduceSpasiboPointsByUser(dto))
            .thenApply(ResponseEntity::ok);
    }

    @ExceptionHandler
    public ResponseEntity<?> handleExceptions(Exception exception) {
        log.error(exception.getMessage(), exception);
        return ResponseEntity.badRequest().body(exception.getMessage());
    }
}
