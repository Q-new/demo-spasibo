package org.gitlab.demospasibo.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@NoArgsConstructor
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    private String name;
    private String password;
    @ManyToOne
    private Role role;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }
}
