package org.gitlab.demospasibo.dto;

import jakarta.validation.constraints.DecimalMin;

public record SpasiboDto(
    Long userId,
    @DecimalMin(value = "0.01", message = "Change cannot be lower than 0.01")
    Double changed
) {
}
