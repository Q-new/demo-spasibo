package org.gitlab.demospasibo.services;

import lombok.AllArgsConstructor;
import org.gitlab.demospasibo.dto.SpasiboDto;
import org.gitlab.demospasibo.exceptions.BadApiException;
import org.gitlab.demospasibo.models.Spasibo;
import org.gitlab.demospasibo.ropsitories.SpasiboRepo;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SpasiboService {
    private final SpasiboRepo spasiboRepo;

    public Double addSpasiboPointsByUser(SpasiboDto spasiboDto) {
        var spasibo = findSpasiboByUserId(spasiboDto.userId());
        spasibo.setPoints(spasibo.getPoints() + spasiboDto.changed());
        return spasiboRepo.save(spasibo).getPoints();
    }

    public Double getSpasiboPointsByUser(Long userId) {
        return findSpasiboByUserId(userId).getPoints();
    }

    public Double reduceSpasiboPointsByUser(SpasiboDto spasiboDto) {
        var spasibo = findSpasiboByUserId(spasiboDto.userId());
        double changed = spasibo.getPoints() - spasiboDto.changed();
        if (changed < 0) {
            throw new BadApiException("Points can`t be below zero");
        }
        spasibo.setPoints(changed);
        return spasiboRepo.save(spasibo).getPoints();
    }

    private Spasibo findSpasiboByUserId(Long id) {
        return spasiboRepo.findByUserId(id)
            .orElseThrow(() -> new BadApiException("Could`t find spasibo for uer with id=" + id));
    }
}
