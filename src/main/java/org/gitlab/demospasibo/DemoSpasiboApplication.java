package org.gitlab.demospasibo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;

@SpringBootApplication(exclude = UserDetailsServiceAutoConfiguration.class)
@EnableMethodSecurity(securedEnabled = true)
public class DemoSpasiboApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpasiboApplication.class, args);
    }

}
