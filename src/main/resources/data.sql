INSERT INTO role(id, role_name) VALUES (1, 'ROLE_CUSTOMER');
INSERT INTO role(id, role_name) VALUES (2, 'ROLE_RECEIVER');
INSERT INTO role(id, role_name) VALUES (3, 'ROLE_BANK');

INSERT INTO user(name, password, role_id) VALUES ('customer1', '$2a$10$aUCR/FfdAG4oCMk4SCxSSO4DKVniGTl6Gcejm837WhVHMpO7NOWdS', 1);
INSERT INTO user(name, password, role_id) VALUES ('customer2', '$2a$10$aUCR/FfdAG4oCMk4SCxSSO4DKVniGTl6Gcejm837WhVHMpO7NOWdS', 1);
INSERT INTO user(name, password, role_id) VALUES ('customer3', '$2a$10$aUCR/FfdAG4oCMk4SCxSSO4DKVniGTl6Gcejm837WhVHMpO7NOWdS', 1);
INSERT INTO user(name, password, role_id) VALUES ('shop', '$2a$10$aUCR/FfdAG4oCMk4SCxSSO4DKVniGTl6Gcejm837WhVHMpO7NOWdS', 2);
INSERT INTO user(name, password, role_id) VALUES ('cinema', '$2a$10$aUCR/FfdAG4oCMk4SCxSSO4DKVniGTl6Gcejm837WhVHMpO7NOWdS', 2);
INSERT INTO user(name, password, role_id) VALUES ('sber', '$2a$10$aUCR/FfdAG4oCMk4SCxSSO4DKVniGTl6Gcejm837WhVHMpO7NOWdS', 3);

INSERT INTO spasibo(points, user_id) VALUES (134, 1);
INSERT INTO spasibo(points, user_id) VALUES (2.56, 2);
INSERT INTO spasibo(points, user_id) VALUES (56794.87, 3);