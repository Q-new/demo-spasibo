package org.gitlab.demospasibo.services;

import org.gitlab.demospasibo.DemoSpasiboApplication;
import org.gitlab.demospasibo.dto.SpasiboDto;
import org.gitlab.demospasibo.exceptions.BadApiException;
import org.gitlab.demospasibo.models.Spasibo;
import org.gitlab.demospasibo.ropsitories.SpasiboRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoSpasiboApplication.class)
class SpasiboServiceTest {

    @Autowired
    private SpasiboService spasiboService;
    @Autowired
    private SpasiboRepo spasiboRepo;

    @Test
    void getSpasiboPointsByUser() {
        double result = spasiboService.getSpasiboPointsByUser(1L);
        Assertions.assertEquals(result, 134.0);
    }

    @Test
    void addSpasiboPointsByUser() {
        Spasibo spasibo = spasiboRepo.findByUserId(1L).get();
        Assertions.assertNotNull(spasibo);
        spasiboService.addSpasiboPointsByUser(new SpasiboDto(1L, 100.0));
        Spasibo changedSpasibo = spasiboRepo.findByUserId(1L).get();
        Assertions.assertEquals(changedSpasibo.getPoints(), 234.0);
    }

    @Test
    void reduceSpasiboPointsByUser() {
        Spasibo spasibo = spasiboRepo.findByUserId(2L).get();
        Assertions.assertNotNull(spasibo);
        spasiboService.reduceSpasiboPointsByUser(new SpasiboDto(2L, 1.0));
        Spasibo changedSpasibo = spasiboRepo.findByUserId(2L).get();
        Assertions.assertEquals(changedSpasibo.getPoints(), 1.56);
        Assertions.assertThrows(
            BadApiException.class,
            () -> spasiboService.reduceSpasiboPointsByUser(new SpasiboDto(2L, 2.0)),
            "Wrong method logics: points could`t be below zero"
        );
    }
}